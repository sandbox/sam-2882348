<?php

namespace Drupal\Tests\entity_menu_position\Unit;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\entity_menu_position\EntityRouteInfo;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Routing\Route;

/**
 * Test the entity route info service.
 *
 * @group entity_menu_position
 * @coversDefaultClass \Drupal\entity_menu_position\EntityRouteInfo
 */
class EntityRouteInfoTest extends UnitTestCase {

  /**
   * @covers ::isCanonicalEntityRoute
   * @dataProvider isCanonicalEntityRouteTestCases
   */
  public function testIsCanonicalEntityRoute($route_name, $is_canonical) {
    $route_match = new RouteMatch($route_name, new Route('/test'));
    $entity_route_info = new EntityRouteInfo($route_match);
    $this->assertEquals($is_canonical, $entity_route_info->isCanonicalEntityRoute());
  }

  /**
   * Test cases for ::testIsCanonicalEntityRoute.
   */
  public function isCanonicalEntityRouteTestCases() {
    return [
      [
        'entity.node.canonical',
        TRUE,
      ],
      [
        'foo.node.canonical',
        FALSE,
      ],
      [
        'entity.node.edit_form',
        FALSE,
      ],
      [
        'some_other_route',
        FALSE,
      ],
    ];
  }

  /**
   * @covers ::getRouteEntityTypeId
   * @dataProvider routeEntityTypeIdTestCases
   */
  public function testGetRouteEntityTypeId($route_name, $entity_type_id) {
    $route_match = new RouteMatch($route_name, new Route('/test'));
    $entity_route_info = new EntityRouteInfo($route_match);
    $this->assertEquals($entity_type_id, $entity_route_info->getRouteEntityTypeId());
  }

  /**
   * Test cases for ::testGetRouteEntityTypeId.
   */
  public function routeEntityTypeIdTestCases() {
    return [
      [
        'entity.node.canonical',
        'node',
      ],
      [
        'foo.node.canonical',
        FALSE,
      ],
      [
        'entity.node.edit_form',
        'node',
      ],
      [
        'some_other_route',
        FALSE,
      ],
    ];
  }

  /**
   * @covers ::getRouteEntityBundleId
   * @dataProvider getRouteEntityBundleIdTestCases
   */
  public function testGetRouteEntityBundleId($route_name, $route_defaults, $route_params, $entity_bundle_id) {
    $route_match = new RouteMatch($route_name, new Route('/test', $route_params), $route_params);
    $entity_route_info = new EntityRouteInfo($route_match);
    $this->assertEquals($entity_bundle_id, $entity_route_info->getRouteEntityBundleId());
  }

  /**
   * Test cases for ::testGetRouteEntityTypeId.
   */
  public function getRouteEntityBundleIdTestCases() {
    $mock_entity = $this->getMock(EntityInterface::class);
    $mock_entity->expects($this->any())->method('bundle')->willReturn('foo');

    return [
      'Canonical route' => [
        'entity.node.canonical',
        [
          'node' => NULL,
        ],
        [
          'node' => $mock_entity,
        ],
        'foo',
      ],
      'Edit form route' => [
        'entity.node.edit_form',
        [
          'node' => NULL,
        ],
        [
          'node' => $mock_entity,
        ],
        'foo',
      ],
      'Missing params' => [
        'entity.node.edit_form',
        [
          'node' => NULL,
        ],
        [],
        FALSE,
      ],
      'Non upcast node' => [
        'entity.node.edit_form',
        [
          'node' => NULL,
        ],
        [
          'node' => 1,
        ],
        FALSE,
      ],
    ];
  }

}

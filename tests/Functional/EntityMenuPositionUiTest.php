<?php

namespace Drupal\Tests\entity_menu_position\Functional;

use Drupal\local_testing\LocalTestingTrait;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the UI.
 *
 * @group entity_menu_position
 */
class EntityMenuPositionUiTest extends BrowserTestBase {

  use LocalTestingTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'entity_menu_position',
    'menu_link_content',
    'node',
    'block',
    'menu_ui',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->drupalLogin($this->drupalCreateUser(['administer menu']));

    NodeType::create([
      'type' => 'foo',
      'name' => 'Foo',
    ])->save();
    NodeType::create([
      'type' => 'bar',
      'name' => 'Bar',
    ])->save();

    $this->drupalPlaceBlock('system_menu_block:main');
  }

  /**
   * Test the UI.
   */
  public function testUi() {
    // Ensure our field is correctly installed.
    $this->assertEmpty(\Drupal::entityDefinitionUpdateManager()->getChangeSummary());

    // Create a menu item with no particular behavior.
    $this->drupalPostForm('admin/structure/menu/manage/main/add', [
      'title[0][value]' => 'Inactive Menu Link',
      'link[0][uri]' => '/node',
    ], 'Save');

    // Create a menu item that is actived by the Node "foo" bundle.
    $this->drupalPostForm('admin/structure/menu/manage/main/add', [
      'title[0][value]' => 'Activated by "foo" nodes',
      'link[0][uri]' => '/node',
      'child_entity_types[0][value]' => 'node:foo',
    ], 'Save');

    // Visiting a node of type Foo will activate one of our menu items.
    $foo_node = Node::create([
      'type' => 'foo',
      'title' => 'Foo Node',
    ]);
    $foo_node->save();
    $this->drupalGet($foo_node->toUrl());
    $this->assertSession()->elementsCount('css', '.menu-item--active-trail', 1);
    $this->assertSession()->elementContains('css', '.menu-item--active-trail', 'Activated by "foo" nodes');

    // Visiting a node of type Bar will not.
    $bar_node = Node::create([
      'type' => 'bar',
      'title' => 'Bar Node',
    ]);
    $bar_node->save();
    $this->drupalGet($bar_node->toUrl());
    $this->assertSession()->elementsCount('css', '.menu-item--active-trail', 0);

    // Create a bar node that is placed in the menu, the normal menu activations
    // should work as expected.
    $bar_node_in_menu = Node::create([
      'type' => 'bar',
      'title' => 'Bar Node in menu',
    ]);
    $bar_node_in_menu->save();
    $this->drupalPostForm('admin/structure/menu/manage/main/add', [
      'title[0][value]' => 'Bar Node in menu',
      'link[0][uri]' => '/node/' . $bar_node_in_menu->id(),
    ], 'Save');
    $this->drupalGet($bar_node_in_menu->tourl());
    $this->assertSession()->elementsCount('css', '.menu-item--active-trail', 1);
    $this->assertSession()->elementContains('css', '.menu-item--active-trail', 'Bar Node in menu');
  }

}

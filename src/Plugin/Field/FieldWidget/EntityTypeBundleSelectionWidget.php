<?php

namespace Drupal\entity_menu_position\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @FieldWidget(
 *   id = "entity_type_bundle_selection_widget",
 *   label = @Translation("Entity Type Bundle Selection Widget"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class EntityTypeBundleSelectionWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Bundle information.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * Constructs a EntityTypeBundleSelectionWidget object.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $bundle_info) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
        '#type' => 'select',
        '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
        '#options' => $this->getValueOptions(),
        '#empty_option' => $this->t('- Select -'),
      ];
    return $element;
  }

  /**
   * Get the value options based on the entities and bundles on the site.
   *
   * @return array
   *   An array of options.
   */
  protected function getValueOptions() {
    $options = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_definition) {
      // Only offer content entities that have a canonical route for now.
      if (!$entity_definition->entityClassImplements(ContentEntityInterface::class) || !$entity_definition->hasLinkTemplate('canonical')) {
        continue;
      }
      // Offer each bundle, and append the name of the bundle to the option if
      // the bundle ID is not the same as the entity ID.
      foreach ($this->bundleInfo->getBundleInfo($entity_definition->id()) as $bundle_id => $bundle) {
        $options[$entity_definition->id() . ':' . $bundle_id] = $entity_definition->getLabel();
        if ($bundle_id !== $entity_definition->id()) {
          $options[$entity_definition->id() . ':' . $bundle_id] .= sprintf(' (%s)', $bundle['label']);
        }
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getName() === 'child_entity_types' && $field_definition->getTargetEntityTypeId() === 'menu_link_content';
  }

}

<?php

namespace Drupal\entity_menu_position;

use Drupal\Core\DestructableInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\menu_link_content\Entity\MenuLinkContent;

/**
 * The MenuActiveTrail class.
 *
 * @see entity_menu_position.services.yml
 */
class MenuActiveTrail implements MenuActiveTrailInterface, DestructableInterface {

  /**
   * The original menu active trail service.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrailInterface
   */
  protected $menuActiveTrail;

  /**
   * Entity information about the current route.
   *
   * @var \Drupal\entity_menu_position\EntityRouteInfo
   */
  protected $entityRouteInfo;

  /**
   * The menu link manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLinkManager;

  /**
   * Content entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $menuLinkContentEntityStorage;

  /**
   * Create an instance of MenuActiveTrail.
   */
  public function __construct(MenuActiveTrailInterface $menu_active_trail, EntityRouteInfo $entity_route_info, MenuLinkManagerInterface $menu_link_manager, EntityStorageInterface $menu_link_content_entity_storage) {
    $this->menuActiveTrail = $menu_active_trail;
    $this->entityRouteInfo = $entity_route_info;
    $this->menuLinkManager = $menu_link_manager;
    $this->menuLinkContentEntityStorage = $menu_link_content_entity_storage;
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveTrailIds($menu_name) {
    $active_trail = ['' => ''];
    if ($active_link = $this->getActiveLink($menu_name)) {
      $active_trail = $this->menuLinkManager->getParentIds($active_link->getPluginId()) + $active_trail;
    }
    return $active_trail;
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveLink($menu_name = NULL) {
    if ($active_link = $this->menuActiveTrail->getActiveLink($menu_name)) {
      return $active_link;
    }
    $entity_type_id = $this->entityRouteInfo->getRouteEntityTypeId();
    $bundle_id = $this->entityRouteInfo->getRouteEntityBundleId();

    if ($entity_type_id && $bundle_id) {
      $query = $this->menuLinkContentEntityStorage->getQuery();
      $query->condition('child_entity_types', "$entity_type_id:$bundle_id");
      $results = $query->execute();

      if ($results) {
        return MenuLinkContent::load(array_shift($results));
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function destruct() {
    if ($this->menuActiveTrail instanceof DestructableInterface) {
      $this->menuActiveTrail->destruct();
    }
  }

}

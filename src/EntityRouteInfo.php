<?php

namespace Drupal\entity_menu_position;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Get information about the current route.
 */
class EntityRouteInfo implements EntityRouteInfoInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Create an instance of EntityRouteInfo.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public function isCanonicalEntityRoute() {
    if ($entity_components = $this->routeEntityComponents()) {
      return $entity_components['entity_route_type'] === 'canonical';
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteEntityTypeId() {
    if ($entity_components = $this->routeEntityComponents()) {
      return $entity_components['entity_type_id'];
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteEntityBundleId() {
    if ($entity_components = $this->routeEntityComponents()) {
      $entity = $this->routeMatch->getParameter($entity_components['entity_type_id']);
      return $entity instanceof EntityInterface ? $entity->bundle() : FALSE;
    }
    return FALSE;
  }

  /**
   * Get components of the route name that relate to entities.
   *
   * @return array|bool
   *   FALSE if the route is not an entity route, or individual components.
   */
  protected function routeEntityComponents() {
    if (preg_match('/(?<first>[\w_]+)\.(?<entity_type_id>[\w_]+)\.(?<entity_route_type>[\w_]+)/', $this->routeMatch->getRouteName(), $matches)) {
      $named_components = array_filter($matches, function($value, $key) {
        return !is_numeric($key);
      }, ARRAY_FILTER_USE_BOTH);
      return $named_components['first'] === 'entity' ? $named_components : FALSE;
    }
    return FALSE;
  }

}

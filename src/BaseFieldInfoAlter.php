<?php

namespace Drupal\entity_menu_position;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class wrapper for hook to alter base fields.
 */
class BaseFieldInfoAlter {

  use StringTranslationTrait;

  /**
   * Implements hook_entity_base_field_info_alter().
   */
  public function alter(&$fields, EntityTypeInterface $entity_type) {
    if ($entity_type->id() !== 'menu_link_content') {
      return;
    }
    $fields['child_entity_types'] = BaseFieldDefinition::create('string')
      ->setLabel($this->t('Child Types'))
      ->setName('child_entity_types')
      ->setDescription($this->t('Select the entity types which will set this menu item to the active trail when visited by users.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setTargetEntityTypeId('menu_link_content')
      ->setDisplayOptions('form', [
        'type' => 'entity_type_bundle_selection_widget',
        'weight' => 10,
      ]);
  }

}

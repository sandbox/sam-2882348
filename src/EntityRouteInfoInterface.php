<?php

namespace Drupal\entity_menu_position;

/**
 * Interface for getting entity information from a route.
 */
interface EntityRouteInfoInterface {

  /**
   * Check if the current route is a canonical entity route.
   *
   * @return bool
   *   If the current route is an entities canonical route.
   */
  public function isCanonicalEntityRoute();

  /**
   * Get the entity type for the current route.
   *
   * @return string|FALSE
   *   The entity type ID matched by the current route.
   */
  public function getRouteEntityTypeId();

  /**
   * Get the bundle ID for the current route.
   *
   * @return string|FALSE
   *   The bundle ID if one exists.
   */
  public function getRouteEntityBundleId();

}
